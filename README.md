# Paragliding Tow Winch

All files relevant to the design and manufacturing of a paragliding tow winch. The goal is to build a small motorized winch that can be controlled by the pilot being launched.