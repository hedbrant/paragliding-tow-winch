# -*- coding: utf-8 -*-

import math
from CustomPrint import CustomPrint
from CustomPlot import CustomPlot
from system_dynamics import System_dynamics
from winch import Winch, Battery, Motor, Drivetrain
from pilot import Pilot
from wind import Wind
import numpy as np
from defs import Indexes as ind, Constants as cnst

rad = cnst.DEG_TO_RAD

# %% Tow model module test

battery_prms =      Battery.Bat48
drivetrain_prms =   Drivetrain.ChainDrive
motor_prms =        Motor.Shaft
pilot_prms =        Pilot.Heavy
glider_prms =       Pilot.Paraglider.SkymanCrossCountry
wind_prms =         Wind.Still

battery =           Battery(battery_prms)
drivetrain =        Drivetrain(drivetrain_prms)
motor =             Motor(motor_prms, battery)
winch =             Winch(motor, drivetrain, battery)
pilot =             Pilot(pilot_prms, glider_prms)
wind =              Wind(wind_prms)

motor_torque_inp = motor.stall_torque / 3
sys_dynamics = System_dynamics(winch, pilot, wind)
data = sys_dynamics.get_motor_torque(10 * rad, 10 * rad)
cpr = CustomPrint()
cpr.print_data(data, 0, "At delta = 10 deg: ")

#%% Plot

cpl = CustomPlot()
cpl.plot(heavy_pilot[0], heavy_pilot[1][0], 'r', "Maximum tow altitude for different pilots", "Drive constant", "125 kg AUW", [0, 0], 0, 0)
cpl.plot(olle_pilot[0], olle_pilot[1][0], 'g', "Maximum tow altitude for different pilots", "Drive constant", "110 kg AUW (Olle)", [0, 0], 1, 0)
cpl.plot(elin_pilot[0], elin_pilot[1][0], 'b', "Maximum tow altitude for different pilots", "Drive constant", "77 kg AUW (Elin)", [0, 1], 1, 0)

#%% Calculate motor char

k_V = 130
volt = 48
max_rpm = k_V * volt
r = np.arange(0.1, 0.2, 0.01)
vel = (max_rpm / 60) * r * 2 * math.pi
print(vel)
