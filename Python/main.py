# -*- coding: utf-8 -*-
#%%
import numpy as np
import math
import statistics as st
from defs import Indexes as ind, Constants as cnst
from CustomPlot import CustomPlot
from CustomPrint import CustomPrint

from system_dynamics import System_dynamics
from system_sim import System_sim
from winch import Winch, Battery, Motor, Drivetrain
from pilot import Pilot
from wind import Wind

rad = cnst.DEG_TO_RAD
deg = cnst.RAD_TO_DEG
rpm = cnst.RADPS_TO_RPM
radps = cnst.RPM_TO_RADPS
kW = cnst.W_TO_KW

#%% Setup
dt = 0.01
time = 200.0
sim_points = int(time / dt + 1)

# Init
battery_prms =      Battery.Bat72
drivetrain_prms =   Drivetrain.Hub
motor_prms =        Motor.QS7kW
pilot_prms =        Pilot.Olle
glider_prms =       Pilot.Paraglider.SkymanCrossCountry
wind_prms =         Wind.Still

battery =           Battery(battery_prms)
drivetrain =        Drivetrain(drivetrain_prms)
motor =             Motor(motor_prms, battery)
winch =             Winch(motor, drivetrain, battery)
pilot =             Pilot(pilot_prms, glider_prms)
wind =              Wind(wind_prms)

#%% Single run
motor_torque_inp = motor.stall_torque / 3.5

sys_dynamics = System_dynamics(winch, pilot, wind)
sys_sim = System_sim(sys_dynamics, motor_torque_inp, dt)

#winch.drivetrain.k_drive = 60#54.35

# Prepare simulation
time_series = np.zeros((ind.LENGTH_EXT, sim_points))
cnt = 0
delta = 0.0
delta_thresh = 80.0
time_thresh = time
time = 0.0

# Loop simulation
while delta < delta_thresh * rad and time < time_thresh:
    time_series[:, cnt] = sys_sim.update_state(motor_torque_inp)[:, 0]
    delta = sys_sim.get_state()[ind.DELTA]
    time = sys_sim.get_state()[ind.T]
    rot_vel = sys_sim.get_state()[ind.ROT_VEL_MOTOR]
    motor_torque_inp = motor.get_max_torque_qs(rot_vel) * 0.86
    cnt = cnt + 1
    
cpl = CustomPlot()
cpl.plot(time_series[ind.X, 0:cnt-1], time_series[ind.Y, 0:cnt-1], 'r', "Distance & Altitude", "Long. distance (m)", "Altitude (m)", [1, 0], 0, 0)
cpl.plot(time_series[ind.X, 0:cnt-1], time_series[ind.DELTA, 0:cnt-1] * deg, 'g', "Tow angle", "Time (s)", "Tow angle (deg)", [1, 0], 1, 1)

[torque_arr, mot_rot_vel_lim_arr, mot_power_limit_arr] = motor.get_limit()
cpl.plot(torque_arr, mot_rot_vel_lim_arr * rpm, 'r', "Motor utilization", "Torque (Nm)", "RPM limit", [0, 0], 0, 0)
cpl.plot(torque_arr, mot_power_limit_arr, 'g', "Motor utilization", "Torque (Nm)", "Power limit (W)", [0, 0], 1, 0)
cpl.plot(time_series[ind.TORQUE_MOTOR, 10:cnt-1], time_series[ind.ROT_VEL_MOTOR, 10:cnt-1] * rpm, 'b', "Motor utilization", "Torque (Nm)", "Motor work region", [0, 0], 1, 1)
cpl.plot(time_series[ind.T, 2:cnt-1], time_series[ind.F_TOW, 2:cnt-1], "r", "Tow force over time", "Time (s)", "Force (N)", [0, 0], 0, 0)
cpl.plot(time_series[ind.T, 2:cnt-1], time_series[ind.V_TOW, 2:cnt-1], "r", "Tow speed over time", "Time (s)", "Speed (m/s))", [0, 0], 0, 0)
cpl.plot(time_series[ind.T, 2:cnt-1], time_series[ind.POWER_OUT, 2:cnt-1] * 1e-3, "r", "Required power over time", "Time (s)", "Power (kW)", [0, 0], 0, 0)

# Print
print("Max altitude: ", '\t\t', round(max(time_series[ind.Y]), 2), " m")
print("Max tow force: ", '\t', round(max(time_series[ind.F_TOW]), 2), " N")
print("Max tow speed: ", '\t', round(max(time_series[ind.V_TOW]), 2), " m/s")
print("Max spool rpm: ", '\t', round(max(time_series[ind.ROT_VEL_SPOOL] * rpm), 2), " rpm")
print("Consumed energy: ", '\t',  round(sum(time_series[ind.CURRENT_IN]) * 0.01 / 3600, 2), "Ah")
print("Consumed energy: ", '\t',  round(sum(time_series[ind.CURRENT_IN]) * 0.01 / 3600 * winch.battery.voltage / 1000, 2), "kWh")

coeffs = np.polyfit(time_series[ind.DELTA, 2:cnt-1], time_series[ind.R_LINE, 2:cnt-1], 2)
est_r = np. zeros(len(time_series[ind.DELTA, 2:cnt-1]))
est_r = drivetrain.r0*np.ones(len(est_r))
est_r = est_r + coeffs[1] * time_series[ind.DELTA, 2:cnt-1]
est_r = est_r + coeffs[0] * np.multiply(time_series[ind.DELTA, 2:cnt-1], time_series[ind.DELTA, 2:cnt-1])
cpl.plot(time_series[ind.DELTA, 2:cnt-1] / rad, time_series[ind.R_LINE, 2:cnt-1], 'b', "Tow angle vs line radius", "Tow angle (deg)", "Line radius (m)", [0, 0], 0, 0)
cpl.plot(time_series[ind.DELTA, 2:cnt-1] / rad, est_r, 'r', "Tow angle vs line radius", "Tow angle (deg)", "Est line radius (m)", [0, 0], 1, 0)

#%% Multiple runs
sys_dynamics = System_dynamics(winch, pilot, wind)

cnt_outer = 0
#drive_constant_arr = np.arange(50, 80, 2)
gear_ratio_arr = np.arange(8.3, 8.7, 0.05)
height_arr = np.zeros((1, len(gear_ratio_arr)))

for gear_ratio in gear_ratio_arr:
        
    motor_torque_inp = motor.stall_torque / 3.5
    sys_sim = System_sim(sys_dynamics, motor_torque_inp, dt)
    winch.drivetrain.gear_ratio = gear_ratio
    
    # Prepare simulation
    time_series = np.zeros((ind.LENGTH_EXT, sim_points))
    cnt = 0
    delta = 0.0
    delta_thresh = 80.0
    time_thresh = time
    time = 0.0   
    
    cnt_outer = cnt_outer + 1
    
    # Loop simulation
    while delta < delta_thresh * rad and time < time_thresh:
        time_series[:, cnt] = sys_sim.update_state(motor_torque_inp)[:, 0]
        delta = sys_sim.get_state()[ind.DELTA]
        time = sys_sim.get_state()[ind.T]
        rot_vel = sys_sim.get_state()[ind.ROT_VEL_MOTOR]
        motor_torque_inp = motor.get_max_torque(rot_vel) * 0.85
        cnt = cnt + 1
    
    height_arr[0, cnt_outer-1] = max(time_series[ind.Y])
    progress = 100 * cnt_outer / len(gear_ratio_arr)
    print("Progress: ", round(progress, 2), " %")

# Plot results
cpl = CustomPlot()
cpl.plot(gear_ratio_arr, height_arr[0], 'r', "Maximum tow altitude", "Gear ratio", "Max altitude (m)", [0, 0], 0, 0)

# Print results
print("Max alt: ", round(max(height_arr[0]), 2), " m")

