# -*- coding: utf-8 -*-

from scipy.optimize import fsolve
import numpy as np
from math import exp, sin, cos, tan, asin, acos, atan
from defs import Indexes as ind, Parameters as prm, Constants as cnst

rad = cnst.DEG_TO_RAD
deg = cnst.RAD_TO_DEG
rho_air = 1.23                          # Air density
g = 9.81                                # Gravity

class System_dynamics:

    def __init__(self, winch, pilot, wind):
        self.pilot = pilot
        self.winch = winch
        self.wind = wind
        
        global motor_eff, batt_volt, r_w, gear_ratio
        motor_eff = self.winch.motor.eff
        batt_volt = self.winch.battery.voltage
    
    def get_glide_angle(self, T_motor, delta):
        dt = self.winch.drivetrain
        pilot = self.pilot
        pg = pilot.glider
        wind = self.wind
        def _eqs_glide_angle(vars):
            beta_a, alpha, v_tas = vars
            eq1 = (pg.c_L0 + pg.c_L_alpha * alpha) * pg.A_proj * rho_air / 2 * v_tas**2 * cos(-beta_a) \
                + (pg.c_D0 + pg.c_D_alpha * alpha) * pg.A_proj * rho_air / 2 * v_tas**2 * sin(-beta_a) \
                + pilot.c_D * pilot.A * rho_air / 2 * v_tas**2 * sin(-beta_a) \
                - pilot.m_t * g \
                - T_motor * dt.k_drive * sin(delta)
            eq2 = (pg.c_L0 + pg.c_L_alpha * alpha) * pg.A_proj * rho_air / 2 * v_tas**2 * sin(-beta_a) \
                - (pg.c_D0 + pg.c_D_alpha * alpha) * pg.A_proj * rho_air / 2 * v_tas**2 * cos(-beta_a) \
                - pilot.c_D * pilot.A * rho_air / 2 * v_tas**2 * cos(-beta_a) \
                + T_motor * dt.k_drive * cos(delta)
            eq3 = (pg.c_D0 + pg.c_D_alpha * alpha) * pg.A_proj * rho_air / 2 * v_tas**2 * pg.l_Gm * cos(alpha) \
                - (pg.c_L0 + pg.c_L_alpha * alpha) * pg.A_proj * rho_air / 2 * v_tas**2 * pg.l_Gm * sin(alpha) \
                - pilot.c_D * pilot.A * rho_air / 2 * v_tas**2 * pg.l_MG * cos(alpha) \
                + pilot.m * g * pg.l_MG * sin(-(alpha + beta_a)) \
                - pg.m * g * pg.l_Gm * sin(-(alpha + beta_a))
                # + T_motor * dt.k_drive * pg.l_MG * sin(delta - (alpha + beta_a))
            return [eq1, eq2, eq3]
        beta_a, alpha, v_tas = fsolve(_eqs_glide_angle, (0, 8 * rad, 10))
        beta_i = atan(v_tas * sin(beta_a) / (v_tas * cos(beta_a) - wind.vel))
        v_i = v_tas * sin(beta_a) / sin(beta_i)
        v_tow = v_i * cos(delta + beta_i)
        F_tow = T_motor * dt.k_drive
        
        battery = self.winch.battery
        motor = self.winch.motor
        
        data = np.zeros((ind.LENGTH, 1))
        data[ind.V_TAS] = v_tas
        data[ind.V_I] = v_i
        data[ind.AOA] = alpha
        data[ind.GLIDE_ANG_A] = beta_a
        data[ind.GLIDE_ANG_I] = beta_i
        data[ind.GLIDE] = 1 / tan(beta_i)
        data[ind.TWIST_ANG] = alpha + beta_a
        data[ind.F_TOW] = F_tow
        data[ind.V_TOW] = v_tow
        data[ind.POWER_OUT] = F_tow * v_tow
        data[ind.POWER_IN] = F_tow * v_tow / motor.eff
        data[ind.CURRENT_IN] = F_tow * v_tow / motor.eff / battery.voltage
        data[ind.TORQUE_SPOOL] = dt.r_w * F_tow
        data[ind.ROT_VEL_SPOOL] = v_tow / dt.r_w
        data[ind.TORQUE_MOTOR] = T_motor
        data[ind.ROT_VEL_MOTOR] = dt.k_drive * v_tow
        return data
    
    def get_motor_torque(self, beta_a, delta):
        dt = self.winch.drivetrain
        pilot = self.pilot
        pg = pilot.glider
        wind = self.wind
        def _eqs_motor_torque(vars):
            T_motor, alpha, v_tas = vars
            eq1 = (pg.c_L0 + pg.c_L_alpha * alpha) * pg.A_proj * rho_air / 2 * v_tas**2 * cos(-beta_a) \
                + (pg.c_D0 + pg.c_D_alpha * alpha) * pg.A_proj * rho_air / 2 * v_tas**2 * sin(-beta_a) \
                + pilot.c_D * pilot.A * rho_air / 2 * v_tas**2 * sin(-beta_a) \
                - pilot.m_t * g \
                - T_motor * dt.k_drive * sin(delta)
            eq2 = (pg.c_L0 + pg.c_L_alpha * alpha) * pg.A_proj * rho_air / 2 * v_tas**2 * sin(-beta_a) \
                - (pg.c_D0 + pg.c_D_alpha * alpha) * pg.A_proj * rho_air / 2 * v_tas**2 * cos(-beta_a) \
                - pilot.c_D * pilot.A * rho_air / 2 * v_tas**2 * cos(-beta_a) \
                + T_motor * dt.k_drive * cos(delta)
            eq3 = (pg.c_D0 + pg.c_D_alpha * alpha) * pg.A_proj * rho_air / 2 * v_tas**2 * pg.l_Gm * cos(alpha) \
                - (pg.c_L0 + pg.c_L_alpha * alpha) * pg.A_proj * rho_air / 2 * v_tas**2 * pg.l_Gm * sin(alpha) \
                - pilot.c_D * pilot.A * rho_air / 2 * v_tas**2 * pg.l_MG * cos(alpha) \
                + pilot.m * g * pg.l_MG * sin(-(alpha + beta_a)) \
                - pg.m * g * pg.l_Gm * sin(-(alpha + beta_a))
                # + T_motor * dt.k_drive * pg.l_MG * sin(delta - (alpha + beta_a))
            return [eq1, eq2, eq3]
        T_motor, alpha, v_tas = fsolve(_eqs_motor_torque, (20, 5 * rad, 10))
        beta_i = atan(v_tas * sin(beta_a) / (v_tas * cos(beta_a) - wind.vel))
        v_i = v_tas * sin(beta_a) / sin(beta_i)
        v_tow = v_i * cos(delta + beta_i)
        F_tow = T_motor * dt.k_drive
        
        motor = self.winch.motor
        battery = self.winch.battery
        
        data = np.zeros((ind.LENGTH, 1))
        data[ind.V_TAS] = v_tas
        data[ind.V_I] = v_i
        data[ind.AOA] = alpha
        data[ind.GLIDE_ANG_A] = beta_a
        data[ind.GLIDE_ANG_I] = beta_i
        data[ind.GLIDE] = 1 / tan(beta_i)
        data[ind.TWIST_ANG] = alpha + beta_a
        data[ind.F_TOW] = F_tow
        data[ind.V_TOW] = v_tow
        data[ind.POWER_OUT] = F_tow * v_tow
        data[ind.POWER_IN] = F_tow * v_tow / motor.eff
        data[ind.CURRENT_IN] = F_tow * v_tow / motor.eff / battery.voltage
        data[ind.TORQUE_SPOOL] = dt.r_w * F_tow
        data[ind.ROT_VEL_SPOOL] = v_tow / dt.r_w
        data[ind.TORQUE_MOTOR] = T_motor
        data[ind.ROT_VEL_MOTOR] = dt.k_drive * v_tow
        return data
    
    def get_drive_constant(self, T_motor, beta_a, delta):
        dt = self.winch.drivetrain
        pilot = self.pilot
        pg = pilot.glider
        wind = self.wind
        def _eqs_drive_constant(vars):
            k_drive, alpha, v_tas = vars
            eq1 = (pg.c_L0 + pg.c_L_alpha * alpha) * pg.A_proj * rho_air / 2 * v_tas**2 * cos(-beta_a) \
                + (pg.c_D0 + pg.c_D_alpha * alpha) * pg.A_proj * rho_air / 2 * v_tas**2 * sin(-beta_a) \
                + pilot.c_D * pilot.A * rho_air / 2 * v_tas**2 * sin(-beta_a) \
                - pilot.m_t * g \
                - T_motor * dt.k_drive * sin(delta)
            eq2 = (pg.c_L0 + pg.c_L_alpha * alpha) * pg.A_proj * rho_air / 2 * v_tas**2 * sin(-beta_a) \
                - (pg.c_D0 + pg.c_D_alpha * alpha) * pg.A_proj * rho_air / 2 * v_tas**2 * cos(-beta_a) \
                - pilot.c_D * pilot.A * rho_air / 2 * v_tas**2 * cos(-beta_a) \
                + T_motor * dt.k_drive * cos(delta)
            eq3 = (pg.c_D0 + pg.c_D_alpha * alpha) * pg.A_proj * rho_air / 2 * v_tas**2 * pg.l_Gm * cos(alpha) \
                - (pg.c_L0 + pg.c_L_alpha * alpha) * pg.A_proj * rho_air / 2 * v_tas**2 * pg.l_Gm * sin(alpha) \
                - pilot.c_D * pilot.A * rho_air / 2 * v_tas**2 * pg.l_MG * cos(alpha) \
                + pilot.m * g * pg.l_MG * sin(-(alpha + beta_a)) \
                - pg.m * g * pg.l_Gm * sin(-(alpha + beta_a))
                # + T_motor * dt.k_drive * pg.l_MG * sin(delta - (alpha + beta_a))
            return [eq1, eq2, eq3]
        k_drive, alpha, v_tas = fsolve(_eqs_drive_constant, (100, 5 * rad, 10))
        beta_i = atan(v_tas * sin(beta_a) / (v_tas * cos(beta_a) - wind.vel))
        v_i = v_tas * sin(beta_a) / sin(beta_i)
        v_tow = v_i * cos(delta + beta_i)
        F_tow = T_motor * dt.k_drive
        
        motor = self.winch.motor
        battery = self.winch.battery
        
        data = np.zeros((ind.LENGTH, 1))
        data[ind.V_TAS] = v_tas
        data[ind.V_I] = v_i
        data[ind.AOA] = alpha
        data[ind.GLIDE_ANG_A] = beta_a
        data[ind.GLIDE_ANG_I] = beta_i
        data[ind.GLIDE] = 1 / tan(beta_i)
        data[ind.TWIST_ANG] = alpha + beta_a
        data[ind.F_TOW] = F_tow
        data[ind.V_TOW] = v_tow
        data[ind.POWER_OUT] = F_tow * v_tow
        data[ind.POWER_IN] = F_tow * v_tow / motor.eff
        data[ind.CURRENT_IN] = F_tow * v_tow / motor.eff / battery.voltage
        data[ind.TORQUE_SPOOL] = dt.r_w * F_tow
        data[ind.ROT_VEL_SPOOL] = v_tow / dt.r_w
        data[ind.TORQUE_MOTOR] = T_motor
        data[ind.ROT_VEL_MOTOR] = dt.k_drive * v_tow
        return data
    