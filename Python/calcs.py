# -*- coding: utf-8 -*-

#%% Inits
from math import pi, sqrt, sin, cos
import numpy as np
from defs import Indexes as ind, Constants as cnst
from scipy.optimize import fsolve
from matplotlib import pyplot as plt

rad = cnst.DEG_TO_RAD
deg = cnst.RAD_TO_DEG
rpm = cnst.RADPS_TO_RPM
radps = cnst.RPM_TO_RADPS
kW = cnst.W_TO_KW

#%% Calculate max stress in spool support tube
F_t = 100
r_w = 0.15
r_2 = 0.075

F_res = F_t * sqrt(1 + (r_w / r_2)**2)
M_b = 3.89

r = 0.0095
t = 0.001
W_b = pi * r**2 * t

sigma_max = M_b / W_b
sigma_tensile = 350e6

k_safety = sigma_tensile / sigma_max

#%% Bolt forces in motor mount

max_chain_force = 1000 * 2 

#%% Number of turns to reel in line

w = 0.098
r0 = 0.15
d_line = 0.003
l0 = 1200.0

kr = d_line**2 / w

# --- Solve second order equation
a = 1
b = r0 / kr
c = - l0 / (2 * pi * kr)

# calculate the discriminant
d = (b**2) - (4*a*c)

# find two solutions
sol1 = (-b-sqrt(d))/(2*a)
sol2 = (-b+sqrt(d))/(2*a)

N = max(sol1, sol2)
R_line = r0 + kr * N
R_added = R_line - r0
R_outer = r0 + R_added * 1.5
print("Outer radius for reeled in line: ", round(R_line, 4), " m")
print("Radius added by line: ", round(R_added, 4), " m", " = ", round(R_added/r0*100), "%")
print("Suggested wall radius: ", round(R_outer, 4), " m")

#%% Lateral speed of line being reeled in

w_spool = 1200 / 60 # rps
d_line = 0.003
v_lat = w_spool * d_line
print("Lateral line speed: ", v_lat * 1000, " mm/s")

#%% Shear force in spool under tow load

t_cyl = 0.001
r_in = 0.032
r_out = 0.08
f_tow = 800

A_in = 2 * t_cyl * r_in * 2 * pi

tau_max = f_tow / A_in

sm_steel = 80e9
sm_alu = 25e9

sf_steel = sm_steel / tau_max
sf_alu = sm_alu / tau_max

print("Max shear stress: ", round(tau_max , 3), " Pa")
print("Safety factor steel: ", round(sf_steel, 3))
print("Safety factor aluminium: ", round(sf_alu, 3))

#%% Reaction forces in mount

Ft = 1000
alpha = pi/2
Fg = 300
r = 0.3
Mt = Ft * r
lh = 0.015
ll = 0.051

def eq(vars):
	Frv, Frh, Frh2 = vars
	eq1 = Frv
	eq2 = Frh - Frh2
	eq3 = Frh*ll + Frh2*lh
	return [eq1, eq2, eq3]

Frv, Frh, Frh2 = fsolve(eq, (Fg - Ft * sin(alpha), -Ft * cos(alpha), Mt))

print(Frv)
print(Frh)
print(Frh2)

upper_left_corner = [10, 10, 10, 4]

# %% Plot gas spring extension as fcn of line bar angle

def d(alpha):
	return sqrt(dh**2 + dv**2 + 2 * l1 * (dv*cos(alpha) + dh*sin(alpha)) + l1**2)

dh = 0.01
dv = 0.045
l1 = 0.2
alpha = np.arange(-pi/2, pi/2, 0.01)

y = [d(a) for a in alpha]
change = [t/max(y)*100 for t in y]

print('l_diff = ', max(y) - min(y))

plt.plot(alpha, y)
plt.show()
plt.plot(alpha, change)
plt.show()

# %% Calc bolt size and distance between bolts of heckpack adapter plate

d = 13
h = 43
t = 4

alpha = 12 * rad
d_new = d * cos(alpha) - t * sin(alpha)
h_new = h * cos(alpha)

print(d_new)
print(h_new)
