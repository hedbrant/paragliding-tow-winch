# -*- coding: utf-8 -*-

class Wind:
    
    def calculate_ground_speed(v_tas, windspeed):
        return v_tas - windspeed
    
    class Still:
        VEL = 0.0
    
    class Calm:
        VEL = 2.5
        
    class Moderate:
        VEL = 5.0
        
    class Strong:
        VEL = 7.5
    
    def __init__(self, params):
        self.vel = params.VEL