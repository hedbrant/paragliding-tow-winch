# -*- coding: utf-8 -*-


from math import exp, sin, cos, tan, asin, acos, atan
import numpy as np
from defs import Indexes as ind, Constants as cnst, Parameters as prm

rad = cnst.DEG_TO_RAD
deg = cnst.RAD_TO_DEG
 
class System_sim:
    
    def __init__(self, sys_dynamics, torque_motor, dt):
        self.state = np.zeros((ind.LENGTH_EXT, 1))
        self.dt = dt
        self.line_length = sys_dynamics.winch.l_tow
        self.tow_x = self.line_length
        self.tow_y = 0.0
        
        delta = 0.0
        self.sys_dynamics = sys_dynamics
        init_state = self.sys_dynamics.get_glide_angle(torque_motor, delta)
        
        for i in range(ind.LENGTH):
            self.state[i] = init_state[i]
            
        self.state[ind.DELTA] = delta
        self.state[ind.X] = 0.0
        self.state[ind.Y] = 0.0
        self.state[ind.LINE_REELED] = 0.0
        self.state[ind.T] = 0.0
        
    def get_state(self):
        return self.state
        
    def update_state(self, torque_motor):
        data = self.sys_dynamics.get_glide_angle(torque_motor, self.state[ind.DELTA])
        self.state[ind.T] = self.state[ind.T] + self.dt
        x = self.state[ind.X] + (data[ind.V_I] * cos(data[ind.GLIDE_ANG_I])) * self.dt
        y = self.state[ind.Y] + (data[ind.V_I] * sin(data[ind.GLIDE_ANG_I])) * self.dt
        self.state[ind.X] = x
        self.state[ind.Y] = y
        line_reeled =self.line_length - np.hypot(self.line_length - x, y)
        self.state[ind.LINE_REELED] = line_reeled
        self.sys_dynamics.winch.drivetrain.set_radius(line_reeled)
        self.state[ind.R_LINE] = self.sys_dynamics.winch.drivetrain.r_w
        self.state[ind.DELTA] = atan(y / (self.tow_x - x))
        
        for i in range(ind.LENGTH):
            self.state[i] = data[i]
            
        return self.state
    