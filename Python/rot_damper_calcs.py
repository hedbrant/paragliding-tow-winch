import numpy as np
from numpy import sin, cos, tan, arcsin, arccos, arctan, hypot, pi, deg2rad
from matplotlib import pyplot as plt

def main():
	dx = 160.0
	dy = 80.0
	l1 = 60.0
	l2 = 35.0

	beta = arctan(l2 / l1)
	l3 = hypot(l1, l2)

	max_ang = deg2rad(45)
	alpha_arr = np.arange(-max_ang, max_ang, 0.01)
	l_res_arr = np.zeros(len(alpha_arr))

	for i in range(len(alpha_arr)):
		alpha = alpha_arr[i]
		p1x = l3 * sin(alpha + beta)
		p1y = -l3 * cos(alpha + beta)
		p2x = dx
		p2y = dy
		l_res_arr[i] = hypot((p2x - p1x), (p2y - p1y))
	
	print("Max l:", round(max(l_res_arr), 2), "mm")
	print("Min l:", round(min(l_res_arr), 2), "mm")
	print("Diff:", round(max(l_res_arr) - min(l_res_arr), 2), "mm")

	plt.figure()
	plt.plot(alpha_arr*180/pi, l_res_arr)
	plt.grid()
	plt.show()

if __name__ == '__main__':
	main()