import numpy as np
from numpy import *
from matplotlib import pyplot as plt
from scipy.optimize import minimize

f_ratio = 1.2	# How much force is in the compressed state l_min compared to ref force at l_max
g = 9.81

class GasStrut:

	def __init__(self, Instance):
		self.l_ext = Instance.l_ext
		self.stroke = Instance.stroke
		self.l_min = Instance.l_ext - Instance.stroke
		self.f_ref = Instance.f_ref

		# Linear spring model, f_ref is at l_ext and f at l_min is f_ref * f_ext_ratio
		self.k = Instance.f_ref * (f_ratio - 1) / self.stroke
		self.m = Instance.f_ref - self.l_ext * self.k

	# Get spring force as fcn of length
	def f(self, l, raiseError = False):
		if raiseError:
			if l > self.l_ext:
				raise ValueError('Gas strut length outside upper limit')
			if l < self.l_min:
				raise ValueError('Gas strut length outside lower limit')
		res = self.m + self.k * l
		return res

class GasStrut_ML5179:
	l_ext = 0.3055
	stroke = 0.121
	f_ref = 45

class GasStrut_8482UN:
	l_ext = 0.28
	stroke = 0.1
	f_ref = 30

class GasStrut_ZSA01056:
	l_ext = 0.169
	stroke = 0.1
	f_ref = 30

class GasStrut_430719106100:
	l_ext = 0.285
	stroke = 0.1
	f_ref = 800

class Kin:

	def __init__(self, Instance):
		self.dx = Instance.dx
		self.dy = Instance.dy
		self.l1 = Instance.l1
		self.l2 = Instance.l2
		self.beta = arctan(Instance.l2 / Instance.l1)
		self.l3 = hypot(Instance.l1, Instance.l2)

	def l_res(self, alpha, dbg = False):
		p1x = self.l3 * sin(alpha + self.beta)
		p1y = -self.l3 * cos(alpha + self.beta)
		p2x = self.dx
		p2y = self.dy
		l_res = hypot((p2x - p1x), (p2y - p1y))
		return l_res

class Kin1:
	dx = 0.154
	dy = 0.157
	l1 = 0.054
	l2 = 0.05

class Dyn:

	def __init__(self, kin, gas_strut):
		self.kin = kin
		self.gas_strut = gas_strut

	def dh(self, alpha):
		theta = arctan(self.kin.l2/self.kin.l1)
		eta = arctan(self.kin.dx/self.kin.dy)
		phi = pi - alpha - theta - eta
		l3 = hypot(self.kin.l1, self.kin.l2)
		l_mid = l3 * sin(phi)
		sigma = arcsin(l_mid / self.kin.l_res(alpha))
		l_mid2 = hypot(self.kin.dx, self.kin.dy)
		dh = l_mid2 * sin(sigma)
		return dh
	
	# Returns resulting counter clockwise torque around rotation axis (stationary part moves), seen from above
	def trq(self, alpha, dbg = False):
		l_res_r = self.kin.l_res(alpha)
		l_res_l = self.kin.l_res(-alpha)
		f_res_r = self.gas_strut.f(l_res_r)
		f_res_l = self.gas_strut.f(l_res_l)
		dh_r = self.dh(alpha)
		dh_l = self.dh(-alpha)
		trq = f_res_l * dh_l - f_res_r * dh_r
		return trq

def dbg1(alpha, gas_strut, kin, dyn):
	d = kin.d(alpha)
	[h, beta, sigma] = kin.h(alpha, dbg=True)
	[trq, fs, fsh, mgmh] = dyn.trq(alpha, dbg=True)
	print('d = ', d)
	print('h = ', h)
	print('beta = ', beta)
	print('sigma = ', sigma)
	print('trq = ', trq)
	print('fs = ', fs)
	print('fsh = ', fsh)
	print('mgmh = ', mgmh)

def dbg(alpha, gas_strut, kin, dyn):
	d = kin.d(alpha)
	res1 = [kin.h(a, dbg=True) for a in alpha]
	res2 = [dyn.trq(a, dbg=True) for a in alpha]
	res1n = np.array(res1)
	res2n = np.array(res2)
	h = res1n[:, 0]
	beta = res1n[:, 1] * 180/pi
	sigma = res1n[:, 2] * 180/pi
	trq = res2n[:, 0]
	fs = res2n[:, 1]
	fsh = res2n[:, 2]
	mgmh = res2n[:, 3]
	diff = fsh - mgmh
	fig, axs = plt.subplots(2, 4)
	alpha = alpha * 180 / pi
	axs[0, 0].plot(alpha, d)
	axs[0, 1].plot(alpha, h)
	axs[0, 2].plot(alpha, beta)
	axs[0, 3].plot(alpha, sigma)
	axs[1, 0].plot(alpha, trq)
	axs[1, 1].plot(alpha, fs)
	axs[1, 2].plot(alpha, fsh)
	axs[1, 2].plot(alpha, -mgmh)
	axs[1, 2].plot(alpha, diff)
	axs[1, 3].plot(alpha, mgmh)
	axs[0, 0].set_title('d')
	axs[0, 1].set_title('h')
	axs[0, 2].set_title('beta')
	axs[0, 3].set_title('sigma')
	axs[1, 0].set_title('trq')
	axs[1, 1].set_title('fs')
	axs[1, 2].set_title('fsh - mgmh')
	axs[1, 3].set_title('mgmh')
	axs[0, 0].grid()
	axs[0, 1].grid()
	axs[0, 2].grid()
	axs[0, 3].grid()
	axs[1, 0].grid()
	axs[1, 1].grid()
	axs[1, 2].grid()
	axs[1, 3].grid()
	plt.show()

def calc_trq1():
	return

def calc_trq(alpha, gas_strut, kin, dyn, ref_trq=None):	
	d = [kin.d(a) for a in alpha]
	trq = [dyn.trq(a, False) for a in alpha]
	#stroke_pos = [(ds - gas_strut.l_min) / gas_strut.l_ext for ds in d]
	stroke_tot = max(d) - min(d)
	fig, axs = plt.subplots(2, 1)
	axs[0].plot(alpha*180/pi, trq)
	if ref_trq is not None:
		axs[0].plot(alpha*180/pi, ref_trq)
		axs[0].legend(['trq', 'ref_trq'])
	axs[0].grid()
	axs[1].plot(alpha*180/pi, d)
	axs[1].plot(alpha*180/pi, gas_strut.l_min * ones(len(alpha)))
	axs[1].plot(alpha*180/pi, gas_strut.l_ext * ones(len(alpha)))
	axs[1].grid()
	plt.show()

def opt_fcn(vrs, alpha, ref_trq, gas_strut):
	[dh, dv, l1] = vrs
	kin = Kin(Kin1)
	kin.dh = dh
	kin.dv = dv
	kin.l1 = l1
	dyn = Dyn(kin, gas_strut)
	trq_dev = [dyn.trq(a) - rt  for a, rt in zip(alpha, ref_trq)]
	cost = np.square(trq_dev).mean()
	return cost

def calc_opt_trq(alpha, gas_strut, kin, dyn):
	gas_strut = GasStrut(GasStrut_8482UN)
	ref_trq = gen_ref_trq(alpha)
	opt_res = minimize(opt_fcn, x0=[0, 0.055, 0.2], bounds=[(0.01, 0.02), (0.04, 0.06), (0.19, 0.21)] , args=(alpha, ref_trq, gas_strut))
	dh = opt_res.x[0]
	dv = opt_res.x[1]
	l1 = opt_res.x[2]
	print("Opt. dh = ", dh)
	print("Opt. dv = ", dv)
	print("Opt. l1 = ", l1)
	kin = Kin(Kin1)
	kin.dh = dh
	kin.dv = dv
	kin.l1 = l1
	dyn = Dyn(kin, gas_strut)
	calc_trq(alpha, gas_strut, kin, dyn, ref_trq)

def gen_ref_trq(alpha):
	ref_trq = np.zeros(len(alpha))
	for val, ang in zip(ref_trq, alpha):
		if ang < 0:
			val = alpha * 180/pi *0.1
	return ref_trq

def main():
	#gas_strut = GasStrut(GasStrut_ML5179)
	#gas_strut = GasStrut(GasStrut_8482UN)
	gas_strut = GasStrut(GasStrut_430719106100)
	kin = Kin(Kin1)
	dyn = Dyn(kin, gas_strut)
	alpha = np.arange(-pi/4, pi/4, 0.01)
	dh_arr = np.zeros(len(alpha))
	trq_arr = np.zeros(len(alpha))
	l_res_arr = np.zeros(len(alpha))
	for i in range(len(alpha)):
		dh_arr[i] = dyn.dh(alpha[i])
		trq_arr[i] = dyn.trq(alpha[i])
		l_res_arr[i] = kin.l_res(alpha[i])

	print("Max l_res: ", round(max(l_res_arr), 3))
	print("Min l_res: ", round(min(l_res_arr), 3))
	print("Stroke: ", round(max(l_res_arr) - min(l_res_arr), 3))

	plt.figure()
	plt.plot(alpha*180/pi, trq_arr)
	plt.ylabel('Torque on axis (Nm)')
	plt.xlabel('Frame rotation angle (deg)')
	plt.grid()
	plt.show()

def test():
	l1 = [0, 20, 50, 30, 2]
	l2 = [0, 20, 50, 30, 2]
	a = [li1 - li2 for li1, li2 in zip(l1, l2)]
	print()

if __name__ == '__main__':
	main()
	#test()
