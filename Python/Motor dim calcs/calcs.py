import pandas as pd
from matplotlib import pyplot as plt
import numpy as np
from numpy import polyfit

kph2ms = 1.0 / 3.6
pi = 3.141592
r_line = 0.15
rpm2kph = r_line * 2 * pi / 1000 * 60
nm2kg = 1 / r_line / 9.82

POLE_PAIRS 	= 16
KV 			= 21.45 #14.3, 8.54

RAD_MIN 	= 0.19
RAD_MAX		= 0.21
RAD_AVG 	= (RAD_MIN + RAD_MAX) / 2

def main():
	kv_calc = 85.0 * kph2ms / (RAD_MIN * 2 * pi) * 60 / 72.0

	vel_max = 14.3 * 48 * RAD_MIN * 2 * pi / 60
	print(vel_max)

def plot_motor_curves():
	file = 'Motor dim calcs/qs_253_v4_7kw.csv'
	df = pd.read_csv(file)
	ax1_names = ['Speed (rpm)']
	ax2_names = ['Voltage (V)', 'Current (A)', 'Efficiency (%)']
	plt.figure()
	for name in ax1_names:
		plt.plot(df['Torque (Nm)'], df[name])
	plt.ylim(0)
	plt.legend(ax1_names)
	plt.grid()
	plt.figure()
	for name in ax2_names:
		plt.plot(df['Torque (Nm)'], df[name])
	plt.legend(ax2_names)
	plt.grid()
	plt.show()

def polyfit_trq_speed():
	file = 'Motor dim calcs/qs_253_v4_7kw.csv'
	df = pd.read_csv(file)
	x = df['Speed (rpm)']
	y = df['Torque (Nm)']
	deg = 6
	c = polyfit(x, y, deg)
	np.save('qs_rpm_trq_params', c)
	print(c)
	x_arr = np.arange(min(x), max(x), 0.1)
	y_arr = c[deg-6] * x_arr**6 + c[deg-5] * x_arr**5 + c[deg-4] * x_arr**4 + c[deg-3] * x_arr**3 + c[deg-2] * x_arr**2 + c[deg-1] * x_arr + c[deg]
	plt.figure()
	plt.plot(x * rpm2kph, y * nm2kg)
	plt.plot(x_arr * rpm2kph, y_arr * nm2kg)
	plt.xlabel('km/h')
	plt.ylabel('kg')
	plt.grid()
	plt.show()



def calc_weight(): # of side walls of hub motor designs
	d_in = 0.33
	d_out = 0.45
	area = ((d_out / 2) ** 2 - (d_in / 2) ** 2) *pi
	vol = area * 0.0025
	weight = vol * 7800
	print(weight)

if __name__ == "__main__":
	polyfit_trq_speed()
	#plot_motor_curves()
	#calc_weight()