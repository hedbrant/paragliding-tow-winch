# -*- coding: utf-8 -*-

from defs import Constants as cnst
import numpy as np
from math import sqrt, pi

rpm = cnst.RADPS_TO_RPM
radps = cnst.RPM_TO_RADPS

class Winch:
    
    def __init__(self, motor, drivetrain, battery):
        self.motor = motor
        self.drivetrain = drivetrain
        self.battery = battery
        self.l_tow = 1250.0
        self.d_line = 0.0025
    
class Motor:
    
    class TurnigySK8:
        MAX_POWER = 4400 * 2
        K_V = 192
        EFF = 0.75
        
    class M80100:
        MAX_POWER = 7000 * 2
        K_V = 130
        EFF = 0
    
    class QS7kW:
        MAX_POWER = 9000
        K_V = 14.3
        EFF = 0.8
    
    def __init__(self, params, battery):
        self.max_power = params.MAX_POWER
        self.max_rot_vel = params.K_V * battery.voltage * radps
        self.eff = params.EFF
        self.k_wT = - self.max_rot_vel**2 / (4 * self.max_power)
        self.stall_torque = - self.max_rot_vel / self.k_wT
        self.coeffs = np.load('qs_rpm_trq_params.npy')
        
    def _get_max_rot_vel(self, torque):
        return self.max_rot_vel + self.k_wT * torque
    
    def get_max_torque(self, rot_vel):
        #return self.stall_torque + 1 / self.k_wT * rot_vel
        return min(self.stall_torque + 1 / self.k_wT * rot_vel, 16.0)
        
    def get_max_torque_qs(self, rot_vel):
        rot_vel = rot_vel * rpm
        c = self.coeffs
        deg = 6
        return c[deg-6] * rot_vel**6 + c[deg-5] * rot_vel**5 + c[deg-4] * rot_vel**4 + c[deg-3] * rot_vel**3 + c[deg-2] * rot_vel**2 + c[deg-1] * rot_vel + c[deg]

    def get_limit(self):
        torque = np.arange(0.0, round(self.stall_torque, 2), 0.01)
        rot_vel_limit = self._get_max_rot_vel(torque)
        power_limit = np.multiply(torque, rot_vel_limit)
        return torque, rot_vel_limit, power_limit
    
    
class Drivetrain:
    
    class Chain:
        T1 = 10             # Nbr of teeth on driving pulley
        T2 = 83             # Nbr of teeth on driving pulley
        R_W = 0.123         # Radius of empty line spool
        W = 0.12            # Width of spool
        D_LINE = 0.0025     # Diameter of line

    class Hub:
        T1 = 1
        T2 = 1
        R_W = 0.15
        W = 0.12
        D_LINE = 0.0025
    
    def __init__(self, params):
        self.r0 = params.R_W
        self.r_w = self.r0
        self.w = 0.12
        self.d_line = 0.0025
        self.k_r = self.d_line**2 / self.w
        self.gear_ratio = params.T2 / params.T1
        self.k_drive = self.gear_ratio / self.r_w
        
    def set_radius(self, line_reeled):
        l0 = line_reeled
        
        # --- Solve second order equation
        a = 1
        b = self.r0 / self.k_r
        c = - l0 / (2 * pi * self.k_r)
        
        # calculate the discriminant
        d = (b**2) - (4*a*c)
        
        # find two solutions
        sol1 = (-b-sqrt(d))/(2*a)
        sol2 = (-b+sqrt(d))/(2*a)
        
        N = max(sol1, sol2)
        self.r_w = self.r0 + self.k_r * N
        self.k_drive = self.gear_ratio / self.r_w
        return
    
class Battery:
    
    class Bat48:
        VOLTAGE = 48.0

    class Bat72:
        VOLTAGE = 72.0
    
    def __init__(self, params):
        self.voltage = params.VOLTAGE
    