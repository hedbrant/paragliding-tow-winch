import numpy as np
from numpy import *
from matplotlib import pyplot as plt
from scipy.optimize import minimize

f_ratio = 1.2	# How much force is in the compressed state l_min compared to ref force at l_max
g = 9.81

class GasStrut:

	def __init__(self, Instance):
		self.l_ext = Instance.l_ext
		self.stroke = Instance.stroke
		self.l_min = Instance.l_ext - Instance.stroke
		self.f_ref = Instance.f_ref

		# Linear spring model, f_ref is at l_ext and f at l_min is f_ref * f_ext_ratio
		self.k = Instance.f_ref * (f_ratio - 1) / self.stroke
		self.m = Instance.f_ref - self.l_ext * self.k

	# Get spring force as fcn of length
	def f(self, l):
		if l > self.l_ext:
			raise ValueError('Gas strut length outside upper limit')
		if l < self.l_min:
			raise ValueError('Gas strut length outside lower limit')
		res = self.m + self.k * l
		return res

class GasStrut_ML5179:
	l_ext = 0.3055
	stroke = 0.121
	f_ref = 45

class GasStrut_8482UN:
	l_ext = 0.28
	stroke = 0.1
	f_ref = 30

class Kin:

	def __init__(self, Instance):
		self.dh = Instance.dh
		self.dv = Instance.dv
		self.l1 = Instance.l1

	# Get spring distance as fcn of line bar angle
	def d(self, alpha):
		return sqrt(self.dh**2 + self.dv**2 + 2 * self.l1 * (self.dv*cos(alpha) + self.dh*sin(alpha)) + self.l1**2)

	def h(self, alpha, dbg = False):
		ph = self.l1 * sin(alpha)
		pv = self.l1 * cos(alpha)
		beta = arctan((ph - self.dh) / (pv + self.dv))
		sigma = alpha - beta
		h = sin(sigma) * self.l1
		if not dbg:
			return h
		else:
			return [h, beta, sigma]

class Kin1:
	dh = 0.015
	dv = 0.06
	l1 = 0.205

class Dyn:

	def __init__(self, Instance, kin, gas_strut):
		self.cog = Instance.cog
		self.m = Instance.m
		self.kin = kin
		self.gas_strut = gas_strut

	def mh(self, alpha):
		return self.cog * sin(alpha)
	
	# Returns resulting clockwise torque of line bar seen from right (pilots perspective)
	def trq(self, alpha, double_strut = True, dbg = False):
		d = self.kin.d(alpha)
		fs = self.gas_strut.f(d)
		if double_strut:
			fs = fs * 2
		h = self.kin.h(alpha)
		mh = self.mh(alpha)
		mgmh = self.m * g * mh
		fsh = fs * h
		trq = fs * h - mgmh
		""" print('d = ', d)
		print('fs = ', fs)
		print('h = ', h)
		print('mg = ', mg)
		print('mh = ', mh)
		print('res = ', res) """
		if not dbg:
			return trq
		else:
			return [trq, fs, fsh, mgmh]

class Dyn1:
	cog = 0.35
	m = 0.216

def dbg1(alpha, gas_strut, kin, dyn):
	d = kin.d(alpha)
	[h, beta, sigma] = kin.h(alpha, dbg=True)
	[trq, fs, fsh, mgmh] = dyn.trq(alpha, dbg=True)
	print('d = ', d)
	print('h = ', h)
	print('beta = ', beta)
	print('sigma = ', sigma)
	print('trq = ', trq)
	print('fs = ', fs)
	print('fsh = ', fsh)
	print('mgmh = ', mgmh)

def dbg(alpha, gas_strut, kin, dyn):
	d = kin.d(alpha)
	res1 = [kin.h(a, dbg=True) for a in alpha]
	res2 = [dyn.trq(a, dbg=True) for a in alpha]
	res1n = np.array(res1)
	res2n = np.array(res2)
	h = res1n[:, 0]
	beta = res1n[:, 1] * 180/pi
	sigma = res1n[:, 2] * 180/pi
	trq = res2n[:, 0]
	fs = res2n[:, 1]
	fsh = res2n[:, 2]
	mgmh = res2n[:, 3]
	diff = fsh - mgmh
	fig, axs = plt.subplots(2, 4)
	alpha = alpha * 180 / pi
	axs[0, 0].plot(alpha, d)
	axs[0, 1].plot(alpha, h)
	axs[0, 2].plot(alpha, beta)
	axs[0, 3].plot(alpha, sigma)
	axs[1, 0].plot(alpha, trq)
	axs[1, 1].plot(alpha, fs)
	axs[1, 2].plot(alpha, fsh)
	axs[1, 2].plot(alpha, -mgmh)
	axs[1, 2].plot(alpha, diff)
	axs[1, 3].plot(alpha, mgmh)
	axs[0, 0].set_title('d')
	axs[0, 1].set_title('h')
	axs[0, 2].set_title('beta')
	axs[0, 3].set_title('sigma')
	axs[1, 0].set_title('trq')
	axs[1, 1].set_title('fs')
	axs[1, 2].set_title('fsh - mgmh')
	axs[1, 3].set_title('mgmh')
	axs[0, 0].grid()
	axs[0, 1].grid()
	axs[0, 2].grid()
	axs[0, 3].grid()
	axs[1, 0].grid()
	axs[1, 1].grid()
	axs[1, 2].grid()
	axs[1, 3].grid()
	plt.show()

def calc_trq1():
	return

def calc_trq(alpha, gas_strut, kin, dyn, ref_trq=None):	
	d = [kin.d(a) for a in alpha]
	trq = [dyn.trq(a, False) for a in alpha]
	#stroke_pos = [(ds - gas_strut.l_min) / gas_strut.l_ext for ds in d]
	stroke_tot = max(d) - min(d)
	line_force = max([abs(t) for t in trq]) / 0.5
	print('Line force: ', round(line_force, 2), 'N')
	fig, axs = plt.subplots(2, 1)
	axs[0].plot(alpha*180/pi, trq)
	if ref_trq is not None:
		axs[0].plot(alpha*180/pi, ref_trq)
		axs[0].legend(['trq', 'ref_trq'])
	axs[0].grid()
	axs[1].plot(alpha*180/pi, d)
	axs[1].plot(alpha*180/pi, gas_strut.l_min * ones(len(alpha)))
	axs[1].plot(alpha*180/pi, gas_strut.l_ext * ones(len(alpha)))
	axs[1].grid()
	plt.show()

def opt_fcn(vrs, alpha, ref_trq, gas_strut):
	[dh, dv, l1] = vrs
	kin = Kin(Kin1)
	kin.dh = dh
	kin.dv = dv
	kin.l1 = l1
	dyn = Dyn(Dyn1, kin, gas_strut)
	trq_dev = [dyn.trq(a) - rt  for a, rt in zip(alpha, ref_trq)]
	cost = np.square(trq_dev).mean()
	return cost

def calc_opt_trq(alpha, gas_strut, kin, dyn):
	gas_strut = GasStrut(GasStrut_8482UN)
	ref_trq = gen_ref_trq(alpha)
	opt_res = minimize(opt_fcn, x0=[0, 0.055, 0.2], bounds=[(0.01, 0.02), (0.04, 0.06), (0.19, 0.21)] , args=(alpha, ref_trq, gas_strut))
	dh = opt_res.x[0]
	dv = opt_res.x[1]
	l1 = opt_res.x[2]
	print("Opt. dh = ", dh)
	print("Opt. dv = ", dv)
	print("Opt. l1 = ", l1)
	kin = Kin(Kin1)
	kin.dh = dh
	kin.dv = dv
	kin.l1 = l1
	dyn = Dyn(Dyn1, kin, gas_strut)
	calc_trq(alpha, gas_strut, kin, dyn, ref_trq)

def gen_ref_trq(alpha):
	ref_trq = np.zeros(len(alpha))
	for val, ang in zip(ref_trq, alpha):
		if ang < 0:
			val = alpha * 180/pi *0.1
	return ref_trq

def main():
	#gas_strut = GasStrut(GasStrut_ML5179)
	gas_strut = GasStrut(GasStrut_8482UN)
	kin = Kin(Kin1)
	dyn = Dyn(Dyn1, kin, gas_strut)
	alpha = np.arange(-pi/2, pi/2, 0.01)
	calc_trq(alpha, gas_strut, kin, dyn)
	#dbg(alpha, gas_strut, kin, dyn)
	#calc_opt_trq(alpha, gas_strut, kin, dyn)

def test():
	l1 = [0, 20, 50, 30, 2]
	l2 = [0, 20, 50, 30, 2]
	a = [li1 - li2 for li1, li2 in zip(l1, l2)]
	print(a)

if __name__ == '__main__':
	main()
	#test()
