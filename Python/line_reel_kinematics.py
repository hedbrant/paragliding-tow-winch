# -*- coding: utf-8 -*-

from math import pi, sqrt, sin, cos, tan, asin, acos, atan 
import numpy as np
import matplotlib.pyplot as plt
'''from .. import defs 
from defs import Indexes as ind, Constants as cnst

rad = cnst.DEG_TO_RAD
deg = cnst.RAD_TO_DEG
rpm = cnst.RADPS_TO_RPM
radps = cnst.RPM_TO_RADPS
kW = cnst.W_TO_KW
'''
#%% Parameters

def get_x(u):
    l = 0.3
    w0 = 0.215
    
    gamma = atan(2 * u / w0)
    w1 = sqrt(4 * u**2 + w0**2)
    theta = acos(w1 / 2 / l)
    d = l * sin(theta)
    x = d * sin(gamma)
    return x

if __name__ == "__main__":
    i = 0
    u_arr = np.linspace(0.0, 0.05, 11)
    x_arr = np.zeros([11, 1])
    for u in u_arr:
        x_arr[i] = get_x(u)
        i = i + 1
    print(x_arr)
    
    plt.figure()
    plt.plot(u_arr * 1000, x_arr * 1000)
    plt.grid()
    plt.ylabel("Offset in output x (mm)")
    plt.xlabel("Offset in input u (mm)")
    plt.show()