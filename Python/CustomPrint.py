# -*- coding: utf-8 -*-
from defs import Indexes as ind, Constants as cnst

rad = cnst.DEG_TO_RAD
deg = cnst.RAD_TO_DEG
rpm = cnst.RADPS_TO_RPM
radps = cnst.RPM_TO_RADPS
kW = cnst.W_TO_KW

class CustomPrint:
    
    def __init__(self):
        return
    
    def print_data(self, data, index, text):
        print(text)
        print("Airspeed: ", '\t\t', round(data[ind.V_TAS][index], 2), " m/s")
        print("Groundspeed: ", '\t', round(data[ind.V_I][index], 2), " m/s")
        print("Alpha: ", '\t\t', round(data[ind.AOA][index] * deg, 2), " degrees")
        print("Beta air: ", '\t\t', round(data[ind.GLIDE_ANG_A][index] * deg, 2), " degrees")
        print("Beta inertial: ", '', round(data[ind.GLIDE_ANG_I][index] * deg, 2), " degrees")
        print("Glide ratio: ", '\t', round(data[ind.GLIDE][index], 2))
        print("Twist angle: ", '\t', round(data[ind.TWIST_ANG][index] * deg, 2), " degrees")
        print("Tow force: ", '\t', round(data[ind.F_TOW][index], 2), " N")
        print("Tow speed: ", '\t', round(data[ind.V_TOW][index], 2), "m/s")
        print("Tow power: ", '\t', round(data[ind.POWER_OUT][index] * kW, 3), " kW")
        print("Spool torque: ", '\t', round(data[ind.TORQUE_SPOOL][index], 2), " Nm")
        print("Spool rpm: ", '\t', round(data[ind.ROT_VEL_SPOOL][index] * rpm, 2), " rpm")
        print("Motor torque: ", '\t', round(max(data[ind.TORQUE_MOTOR]), 2), " Nm")
        print("Motor rpm: ", '\t', round(max(data[ind.ROT_VEL_MOTOR]) * rpm, 2), "rpm")
        print("Drive constant: ", round(data[ind.K_DRIVE][index], 2))
        print("Power supply: ", '\t', round(data[ind.POWER_IN][index] * kW, 2), " kW")
        print("Current supply: ", round(data[ind.CURRENT_IN][index], 2), " A")
        print("Consumed energy: ", round(sum(data[ind.CURRENT_IN] * 0.01), 2), " Ah")
        print('\n')