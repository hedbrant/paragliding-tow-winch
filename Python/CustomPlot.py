# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt

class CustomPlot:
    
    def __init__(self):
        self.cnt = 0
        self.plt = plt
    
    # Form: cp.plot(x, y, color, title, xlabel, ylabel, axis_lim, hold, change_labels)
    def plot(self, x, y, color, title, xlabel, ylabel, axis_lim, hold, change_labels):
        if self.cnt == 0 or hold == 0:
            plt.figure()
            plt.plot(x, y, color)
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)
            self.legend = []
            self.legend.append(ylabel)
            self.plt.grid()
        else:
            plt.plot(x, y, color)
            self.legend.append(ylabel)
            if change_labels == 1:
                title = "Multiple plots"
                ylabel = "Multiple quantities"
                plt.ylabel(ylabel)
        if axis_lim[0] == 1:
            plt.gca().set_xlim(left=0)
        if axis_lim[1] == 1:
            plt.gca().set_ylim(bottom=0)
        plt.title(title)
        plt.legend(self.legend)
        self.cnt = self.cnt + 1
    