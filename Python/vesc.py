
from pyvesc import VESC
import time
import serial
import threading

class VescHandler(threading.Thread):
    
    def __init__(self, ser_vesc): #, state_estimator):
        threading.Thread.__init__(self)
        self.vesc = VESC(ser_vesc)
        #self.state_estimator = state_estimator
        self.vel_cmd = 0.0
        #self.steer_mid_pos = 0.55
        #self.steer_cmd = self.steer_mid_pos
        self.go_ok = False
        
    def set_vel(self, new_vel):
        self.vel_cmd = new_vel
        
    def set_steering(self, new_steer):
        self.steer_cmd = new_steer
        
    def run(self):
        while True:
            if self.go_ok is True:
                rpm = int(round(self.vel_cmd * 4533))
                self.vesc.set_rpm(rpm)
                #self.vesc.set_servo(self.steer_cmd)
                #self.state_estimator.vel = self.vel_cmd
            else:
                self.vesc.set_rpm(0)
                #self.vesc.set_servo(self.steer_mid_pos)
                #self.state_estimator.vel = 0.0
            time.sleep(0.05)
            