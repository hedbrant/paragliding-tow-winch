from pyvesc import VESC
import time
import serial
from serial.tools import list_ports
import numpy as np

if __name__ == '__main__':

	ser_vesc = 'COM3'
	vesc = VESC(ser_vesc)

	#vesc.set_duty_cycle(0.15)
	vesc.set_rpm(2000)
	time.sleep(2.0)

	while True:
		print(str(vesc.get_measurements().rpm))
		time.sleep(1.0)