# -*- coding: utf-8 -*-
import math

class Constants:
    DEG_TO_RAD = math.pi / 180
    RAD_TO_DEG = 180 / math.pi
    RADPS_TO_RPM = 60 / (2 * math.pi)
    RPM_TO_RADPS = (2 * math.pi) / 60
    W_TO_KW = 0.001
    G = 9.81

class Indexes:
    # For the model
    V_TAS = 0
    V_I = 1
    AOA = 2
    GLIDE_ANG_A = 3
    GLIDE_ANG_I = 4
    GLIDE = 5
    TWIST_ANG = 6
    F_TOW = 7
    V_TOW = 8
    POWER_OUT = 9
    POWER_IN = 10
    CURRENT_IN = 11
    TORQUE_SPOOL = 12
    ROT_VEL_SPOOL = 13
    TORQUE_MOTOR = 14
    ROT_VEL_MOTOR = 15
    K_DRIVE = 16
    LENGTH = 17
    
    # For the extended simulation model
    DELTA = 17
    X = 18
    Y = 19
    LINE_REELED = 20
    R_LINE = 21
    T = 22
    LENGTH_EXT = 23
    
class Parameters:

    # Aerodynamic coefficients
    '''
    # Modeling paper
    c_L0 = 0.08
    c_L_alpha = 0.065 * deg
    c_Dg0 = 0.0375
    c_Dg_alpha = 0.0067 * deg
    c_Dp = 0.46
    # Aerodynamics paper
    c_L0 = 0.0
    c_L_alpha = 0.008 * deg
    c_Dg0 = 0.02
    c_Dg_alpha = 0.00143 * deg
    c_Dp = 0.46
    # Aero paper 2: https://www.jstage.jst.go.jp/article/tjsass/49/163/49_163_9/_pdf
    c_L0 = 0.0
    c_L_alpha = 0.15 * deg
    c_Dg0 = 0.05
    c_Dg_alpha = 0.04 * deg
    c_Dp = 0.46
    '''

