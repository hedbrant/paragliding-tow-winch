# -*- coding: utf-8 -*-

from defs import Constants as cnst

rad = cnst.DEG_TO_RAD
deg = cnst.RAD_TO_DEG
g = cnst.G

class Pilot:
    
    class Heavy:
        M = 120.5                           # Pilot + equipment mass
        A = 1.0                             # Pilot area
        c_D = 0.46                          # Coeff. of drag
        
    class Olle:
        M = 110.0                           # Pilot + equipment mass
        A = 1.0                             # Pilot area
        c_D = 0.46                          # Coeff. of drag
        
    class Elin:
        M = 72.0                            # Pilot + equipment mass
        A = 0.8                             # Pilot area
        c_D = 0.46                          # Coeff. of drag
        
    def __init__(self, pilot_prms, glider_prms):
        if pilot_prms is Pilot.Elin:
            glider_prms = Pilot.Paraglider.GinBolero5
        if pilot_prms is Pilot.Olle:
            gliderglider_prms = Pilot.Paraglider.SkymanCrossCountry
        self.m = pilot_prms.M
        self.F_W = self.m * g
        self.A = pilot_prms.A
        self.c_D = pilot_prms.c_D
        self.m_t = self.m + glider_prms.m
        self.glider = Pilot.Paraglider(glider_prms, self)
    
    class Paraglider:
    
        class SkymanCrossCountry:
            m =             4.5             # Glider mass
            l_line =        7.72            # Line length
            A_proj =        24.02           # Projected wing area
            c_L0 =          0.12            # Aero
            c_L_alpha =     0.125 * deg     # Aero
            c_D0 =          0.0235          # Aero
            c_D_alpha =     0.0072 * deg    # Aero
            
        class GinBolero5:
            m =             4.7             # Glider mass
            l_line =        6.72            # Line length
            A_proj =        19.29           # Projected wing area
            c_L0 =          0.12            # Aero
            c_L_alpha =     0.125 * deg     # Aero
            c_D0 =          0.0235          # Aero
            c_D_alpha =     0.0072 * deg    # Aero
        
        def __init__(self, glider_prms, pilot):
            self.m = glider_prms.m
            self.l_line = glider_prms.l_line
            self.A_proj = glider_prms.A_proj
            self.c_L0 = glider_prms.c_L0
            self.c_L_alpha = glider_prms.c_L_alpha
            self.c_D0 = glider_prms.c_D0
            self.c_D_alpha = glider_prms.c_D_alpha
            self.l_MG = self.m * self.l_line / pilot.m_t
            self.l_Gm = self.l_line - self.l_MG
    
    
        