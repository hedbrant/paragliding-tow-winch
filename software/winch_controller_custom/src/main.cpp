/*
 * receiver
 * receives target pull value from lora link
 * sends acknowlegement on lora with current parameters
 * writes target pull with PWM signal to vesc
 * reads current parameters (tachometer, battery %, motor temp) with UART from vesc, based on (https://github.com/SolidGeek/VescUart/)
 *
 */

// vesc battery number of cells
static int numberOfCells = 16;
static int myMaxPull = 85; // 0 - 127 [kg], must be scaled with VESC ppm settings

#include "LiPoCheck.h" //to calculate battery % based on cell Voltage

#include <Pangodream_18650_CL.h>
#include <SPI.h>
#include <LoRa.h>
// #include <Wire.h>  // replaced with input from SSD1306DrawingDemo.ino
// #include "SSD1306.h" // replaced with input from SSD1306DrawingDemo.ino

// Etienne: the following is taken from SSD1306DrawingDemo.ino:
// Include the correct display library
// For a connection via I2C using Wire include
#include <Wire.h>		 // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306Wire.h" // legacy include: `#include "SSD1306.h"

// Initialize the OLED display using Wire library
SSD1306Wire display(0x3c, SDA, SCL); // ADDRESS, SDA, SCL - SDA and SCL usually populate automatically based on your board's pins_arduino.h e.g. https://github.com/esp8266/Arduino/blob/master/variants/nodemcu/pins_arduino.h
// End stuff taken from SS1306DrawingDemo.io

#define SCK 5	   // GPIO5  -- SX1278's SCK
#define MISO 19	   // GPIO19 -- SX1278's MISnO
#define MOSI 27	   // GPIO27 -- SX1278's MOSI
#define SS 18	   // GPIO18 -- SX1278's CS
#define RST 14	   // GPIO14 -- SX1278's RESET
#define DI0 26	   // GPIO26 -- SX1278's IRQ(Interrupt Request)
#define BAND 433E6 // frequency in Hz (433E6, 868E6, 915E6)

// SSD1306 display(0x3c, 21, 22); //replaced with the above from SSD1306DrawingDemo.ino
int rssi = 0;
float snr = 0;
String packSize = "--";
String packet;

#include <rom/rtc.h>
#include "Arduino.h"
#include <Button2.h>

// battery measurement
// #define CONV_FACTOR 1.7
// #define READS 20
Pangodream_18650_CL BL(35); // pin 34 old / 35 new v2.1 hw

// Using VescUart librarie to read from Vesc (https://github.com/SolidGeek/VescUart/)
#include <VescUart.h>
#define VESC_RX 14 // connect to TX on Vesc
#define VESC_TX 2  // connect to RX on Vesc
VescUart vescUART;

// PWM signal to vesc
#define PWM_PIN_OUT 13		// Define Digital PIN
#define PWM_TIME_0 950.0	// PWM time in ms for 0% , PWM below will be ignored!! need XXX.0!!!
#define PWM_TIME_100 2000.0 // PWM time in ms for 100%, PWM above will be ignored!!

#define T_SCREEN 100200
#define T_RX_DROPOUT 600020
#define P_PWM 20000

static int loopStep = 0;
static uint8_t activeTxId = 0;
uint8_t rx_cnt;

void pulseOut(int pin, int us);
double exp_filter(double new_val, double curr_val, double coeff);
void update_screen();

#include "common.h"
struct RemoteToWinchData r2w;
struct LoraRxMessage loraRxMessage;

int smoothStep = 0;		   // used to smooth pull changes
int hardBrake = -20;	   // in kg
int softBrake = -8;		   // in kg
int defaultPull = 8;	   // in kg
int prePullScale = 20;	   // in % of myMaxPull
int takeOffPullScale = 50; // in % of myMaxPull
int fullPullScale = 80;	   // in % of myMaxPull
int strongPullScale = 100; // in % of myMaxPull

int currentId = 0;
int currentState = -1;
// pull value send to VESC --> default soft brake
// defined as int to allow smooth changes without overrun
int currentPull = softBrake; // active range -127 to 127
int8_t targetPullValue = 0;	 // received from lora transmitter or rewinding winch mode

uint8_t vescBattery = 0;
uint8_t vescTempMotor = 0;
unsigned long lastTxLoraMessageMillis = 0;
unsigned long previousTxLoraMessageMillis = 0;
unsigned long lastRxLoraMessageMillis = 0;
unsigned long previousRxLoraMessageMillis = 0;
uint32_t pwmReadTimeValue = 0;
uint32_t pwmWriteTimeValue = 0;
unsigned long lastWritePWMMillis = 0;
unsigned int loraErrorCount = 0;
unsigned long loraErrorMillis = 0;

unsigned long t_now, t_prev;
unsigned long t_prev_screen, t_prev_rx, t_prev_pwm;
unsigned long p_rx;

bool lora_connection;

void setup()
{
	Serial.begin(115200);

	// Setup UART port for Vesc communication
	Serial1.begin(115200, SERIAL_8N1, VESC_RX, VESC_TX);
	vescUART.setSerialPort(&Serial1);
	// vescUART.setDebugPort(&Serial);

	////OLED display // replaced with stuff from SSD1306DrawingDemo.ino
	// pinMode(16,OUTPUT);
	// pinMode(2,OUTPUT);
	// digitalWrite(16, LOW);    // set GPIO16 low to reset OLED
	// delay(50);
	// digitalWrite(16, HIGH); // while OLED is running, must set GPIO16 in high

	// lora init
	SPI.begin(SCK, MISO, MOSI, SS);
	LoRa.setPins(SS, RST, DI0);
	if (!LoRa.begin(BAND))
	{
		Serial.println("Starting LoRa failed!");
		while (1)
			;
	}
	LoRa.setTxPower(20, PA_OUTPUT_PA_BOOST_PIN);
	// LoRa.setSpreadingFactor(10);   // default is 7, 6 - 12
	LoRa.enableCrc();
	// LoRa.setSignalBandwidth(500E3);   //signalBandwidth - signal bandwidth in Hz, defaults to 125E3. Supported values are 7.8E3, 10.4E3, 15.6E3, 20.8E3, 31.25E3, 41.7E3, 62.5E3, 125E3, 250E3, and 500E3.

	// display init
	display.init();
	// display.flipScreenVertically();

	// PWM Pins
	// pinMode(PWM_PIN_IN, INPUT);
	pinMode(PWM_PIN_OUT, OUTPUT);

	display.clear();
	display.setTextAlignment(TEXT_ALIGN_LEFT);
	display.setFont(ArialMT_Plain_10);
	Serial.printf("Starting Receiver \n");
	display.drawString(0, 0, "Starting Receiver");
}

void loop()
{
	t_now = micros();

	loopStep++;

	// LoRa data available?
	// packet from transmitter
	if (LoRa.parsePacket() == sizeof(r2w))
	{
		rx_cnt++;
		LoRa.readBytes((uint8_t *)&r2w, sizeof(r2w));
		// allow only one ID to control the winch at a given time
		// after 5 seconds without a message, a new id is allowed

		targetPullValue = r2w.setTorque;
		currentId = r2w.id;
		currentState = r2w.currentState;
		previousTxLoraMessageMillis = t_prev_rx; // remember time of previous paket
		p_rx = t_now - t_prev_rx;
		t_prev_rx = t_now;
		rssi = LoRa.packetRssi();
		snr = LoRa.packetSnr();
		Serial.printf("Value received: %d, RSSI: %d: , SNR: %d \n", r2w.setTorque, rssi, snr);
	}

	// LoRa dropout handler
	if (t_now - t_prev_rx > T_RX_DROPOUT)
	{
		targetPullValue = 0;
		lora_connection = false;
	}
	else{
		lora_connection = true;
	}

	// Write to VESC PPM input
	if (t_now - t_prev_pwm >= P_PWM)
	{
		t_prev_pwm = t_now;
		pwmWriteTimeValue = constrain((targetPullValue + 127), 0, 255) * (PWM_TIME_100 - PWM_TIME_0) / 254 + PWM_TIME_0;
		pulseOut(PWM_PIN_OUT, pwmWriteTimeValue);
	}

	// UPDATE SCREEN
	if (t_now - t_prev_screen >= T_SCREEN)
	{
		update_screen();
		t_prev_screen = t_now;
	}
}

void pulseOut(int pin, int us)
{
	digitalWrite(pin, HIGH);
	us = max(us - 20, 1); // biase caused by digital write/read
	delayMicroseconds(us);
	digitalWrite(pin, LOW);
}

void update_screen()
{
	display.clear();
	display.setTextAlignment(TEXT_ALIGN_LEFT);
	display.setFont(ArialMT_Plain_16); // 10, 16, 24
	display.drawString(0, 0, "rx val: " + String(r2w.setTorque));
	display.setFont(ArialMT_Plain_16); // 10, 16, 24
	display.drawString(0, 12, "p_rx: " + String((int) p_rx*0.001));
	display.setFont(ArialMT_Plain_16); // 10, 16, 24
	display.drawString(0, 24, "set val: " + String(targetPullValue));
	display.setFont(ArialMT_Plain_16); // 10, 16, 24
	if (lora_connection){
		display.drawString(0, 36, "conn: true");
	} else {
		display.drawString(0, 36, "conn: false");
	}
	display.display();
}

double exp_filter(double new_val, double curr_val, double coeff)
{
	return new_val * (1.0 - coeff) + curr_val * coeff;
}
