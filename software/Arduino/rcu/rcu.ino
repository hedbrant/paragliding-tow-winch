#include "math.h"
#include <SoftwareSerial.h>
#include "EBYTE.h"
#include <VescUart.h>

#define PIN_RX      2
#define PIN_TX      3
#define PIN_M0      4
#define PIN_M1      5
#define PIN_AX      6
#define PIN_PWM_IN  7

#define TX_SAMPLE_TIME        100000
#define PWM_DROPOUT_PERIOD    80000
#define PWM_CYCLE_THRESH      20000
#define PWM_CYCLE_TIME_CONST  400000

#define MODE_RUNNING          0
#define MODE_CALIBRATING      1

unsigned long t_start, t_h, t_pwm_last, t_pwm_diff, t_pwm_diff_f, t_last, t_now, t_temp;
byte throttle, mode;
volatile unsigned long t_rise, t_fall, t_pwm_now;
volatile bool pwm_flag, pwm_state, pwm_state_prev;
bool tx_timer, tx_enable, pwm_timer_flag, pwm_active, pwm_dropout;
float PWM_CYCLE_FILTCOEFF;
byte nbr;

SoftwareSerial ESerial(PIN_RX, PIN_TX);
EBYTE Transceiver(&ESerial, PIN_M0, PIN_M1, PIN_AX);
VescUart vesc;


void setup() {
  // General 
  Serial.begin(9600);
  mode = 0;
  PWM_CYCLE_FILTCOEFF = exp(float (-TX_SAMPLE_TIME) / float (PWM_CYCLE_TIME_CONST));
  nbr = 0;
  
  // PWM
  pinMode(PIN_PWM_IN, INPUT);
  pwm_state = digitalRead(PIN_PWM_IN);
  pwm_state_prev = pwm_state;
  t_now = micros(); 
  t_last = t_now;
  t_pwm_last = t_last - PWM_DROPOUT_PERIOD;

  // VESC
  vesc.setSerialPort(&ESerial);

  // LoRa
  ESerial.begin(9600);
  Transceiver.init();
  Transceiver.PrintParameters();
  Serial.print("Filter coeff: "); Serial.println(PWM_CYCLE_FILTCOEFF);

  t_start = micros();
}

// ---- MAIN ----
void loop() {
  process_tx_timer();   // Sets tx_timer to true periodically
  
  pwm_poll();
  process_pwm();
  if (tx_timer && tx_enable && pwm_active){
    send_throttle_val();
  }

  if (tx_timer) {
    Serial.print(">diff:"); Serial.println(t_pwm_diff);
    Serial.print(">diff_f:"); Serial.println(t_pwm_diff_f);
    Serial.print(">throttle:"); Serial.println(throttle);
    nbr = 127 + 126 * sin((micros() - t_start) * 2 * 3.14159 * 1.0 * 0.000001);
  }

  /*mode_selection();

  switch (mode) {
    case MODE_RUNNING:
      t_now = micros();
      process_tx_timer();
      process_pwm();
      process_pwm_timer();

      if (tx_timer && tx_enable && pwm_active){
        send_throttle_val();
      }

      if(tx_timer){
        Serial.println(t_pwm_diff_f);
      }

      tx_timer = false;
      break;
      
    case MODE_CALIBRATING:
      break;
  }*/
}

// ---- FUNCTIONS ----
void mode_selection() {
  ;
}

void process_tx_timer() {
  tx_timer = false;
  t_now = micros();
  unsigned long diff = t_now - t_last;
  if (diff >= TX_SAMPLE_TIME){
      t_last = t_now;
      tx_timer = true;
    }
}

/*----- LoRa communication -----*/
void send_throttle_val(){
  throttle = (t_h - 920) * 0.21;
  Transceiver.SendByte(throttle);
}

/*----- PWM reading -----*/
void process_pwm() {
  if (pwm_flag){
    pwm_active = true;
    pwm_dropout = false;
    t_h = t_fall - t_rise;
  } else if ((t_now - t_pwm_last) >= PWM_DROPOUT_PERIOD) {
    pwm_active = false;
    pwm_dropout = true;
  }

  if (pwm_flag || pwm_dropout) {
    pwm_flag = false;
    pwm_dropout = false;

    t_pwm_diff = t_now - t_pwm_last;
    t_pwm_last = t_now;
  }

  if (tx_timer) {
    t_pwm_diff_f = t_pwm_diff_f * PWM_CYCLE_FILTCOEFF + t_pwm_diff * (1 - PWM_CYCLE_FILTCOEFF);
  }

  if (t_pwm_diff_f < PWM_CYCLE_THRESH){
    tx_enable = true;
  } else{
    tx_enable = false;
  }
}

// Temp fcn for polling pwm becasue of limited amount of concurrent PCINT pins
void pwm_poll(){
  pwm_state = digitalRead(PIN_PWM_IN);
  if(pwm_state != pwm_state_prev){
    pwm_state_prev = pwm_state;
    pwm_state_change();
  }
}

// PWM pin change interrupt
void pwm_state_change(){
  if (pwm_state){
    t_rise = micros();
  }else{
    t_fall = micros();
    pwm_flag = true;
  }
}