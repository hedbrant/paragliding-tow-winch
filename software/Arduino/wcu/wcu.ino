#include <SoftwareSerial.h>
#include "EBYTE.h"
#include <VescUart.h>

#define PIN_RX 2
#define PIN_TX 3
#define PIN_M0 4
#define PIN_M1 5
#define PIN_AX 6

#define RX_SAMPLE_TIME                  100000
#define VESC_TX_SAMPLE_TIME             20000
#define VESC_TX_FILTER_TIME_CONSTANT    1.35 * RX_SAMPLE_TIME
#define RX_DROPOUT_THRESH               1.5 * RX_SAMPLE_TIME

unsigned long   t_now, t_last, t_last_rx;
bool            tx_timer, rx_flag, rx_dropout;
byte            u;
float           VESC_TX_FILTER_COEFF;
int             vel, vel_f;

SoftwareSerial  ESerial(PIN_RX, PIN_TX);
EBYTE           Transceiver(&ESerial, PIN_M0, PIN_M1, PIN_AX);
VescUart        vesc;

void setup() {
  // General
  Serial.begin(115200);
  VESC_TX_FILTER_COEFF = exp(float (-VESC_TX_SAMPLE_TIME) / float (VESC_TX_FILTER_TIME_CONSTANT));
  Serial.print('Vesc filter coeff: ');
  Serial.println(VESC_TX_FILTER_COEFF);

  // LoRa
  ESerial.begin(9600);
  Serial.println("Starting Reader");
  Transceiver.init();
  Transceiver.PrintParameters();

  // VESC
  vesc.setSerialPort(&Serial);

  t_last = micros(); 
  t_now = t_last;
  t_last_rx = t_now - RX_DROPOUT_THRESH;
}

void loop() {
  process_vesc_tx_timer();
  process_ebyte_rx();
  process_vesc_tx();
}

void process_vesc_tx_timer() {
  tx_timer = false;
  t_now = micros();
  unsigned long diff = t_now - t_last;
  if (diff >= VESC_TX_SAMPLE_TIME){
      t_last = t_now;
      tx_timer = true;
    }
}

void process_ebyte_rx() {
  if (ESerial.available()) {
    u = Transceiver.GetByte();
    rx_flag = true;
    t_last_rx = micros();
  }
  rx_dropout = (t_now - t_last_rx >= RX_DROPOUT_THRESH) ? true : false;
}

void process_vesc_tx() {
  if (tx_timer && !rx_dropout) {
    vel = u * 40;
    vel_f = vel_f * VESC_TX_FILTER_COEFF + vel * (1 - VESC_TX_FILTER_COEFF);
    vesc.setRPM(vel_f);
    Serial.print(">rx_dropout:"); Serial.println(rx_dropout);
    Serial.print(">vel:"); Serial.println(vel);
    Serial.print(">vel_f:"); Serial.println(vel_f);
  }
}
