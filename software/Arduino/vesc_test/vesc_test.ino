#include "SoftwareSerial.h"
#include <VescUart.h>

#define PIN_RX  2 
#define PIN_TX  3

SoftwareSerial ESerial(PIN_RX, PIN_TX);
VescUart UART;
bool led_state;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
  ESerial.begin(9600);
  UART.setSerialPort(&ESerial);
  while(!ESerial){;}
  Serial.println("Init done");
}

void loop() {
  UART.setRPM(2000);
  delay(200);
  led_state = !led_state;
  digitalWrite(LED_BUILTIN, led_state);
}
