#include <Arduino.h>

#include "SSD1306Wire.h" // legacy include: `#include "SSD1306.h"`

SSD1306Wire display(0x3c, SDA, SCL);
// SSD1306 display(0x3c, 21, 22); //replaced with the above from SSD1306DrawingDemo.ino
int rssi = 0;
float snr = 0;
String packSize = "--";
String packet ;

void setup() {
  display.init();
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  display.drawString(0, 0, "Starting Receiver");
  display.display();
}

void loop() {
  // put your main code here, to run repeatedly:
}