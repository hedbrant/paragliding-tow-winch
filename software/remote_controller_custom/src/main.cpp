/* Original code
 *
 * transmitter
 * sends current state (pull value)
 * receives acknowlegement with current parameters
 * communication is locked to a specific transmitter for 5 seconds after his last message
 * admin ID 0 can always take over communication
 */

static int myID = 0;	   // set to your desired transmitter id, "0" is for admin 1 - 15 is for additional transmitters [unique number from 1 - 15]
static int myMaxPull = 85; // 0 - 127 [kg], must be scaled with VESC ppm settings

#include <Pangodream_18650_CL.h>
#include <SPI.h>
#include <LoRa.h>
// #include <Wire.h>  // replaced with input from SSD1306DrawingDemo.ino
// #include "SSD1306.h" // replaced with input from SSD1306DrawingDemo.ino

// Etienne: the following is taken from SSD1306DrawingDemo.ino:
// Include the correct display library
// For a connection via I2C using Wire include
#include <Wire.h>		 // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306Wire.h" // legacy include: `#include "SSD1306.h"`

// Initialize the OLED display using Wire library
SSD1306Wire display(0x3c, SDA, SCL); // ADDRESS, SDA, SCL - SDA and SCL usually populate automatically based on your board's pins_arduino.h e.g. https://github.com/esp8266/Arduino/blob/master/variants/nodemcu/pins_arduino.h
// End stuff taken from SS1306DrawingDemo.io

#define SCK 5	   // GPIO5  -- SX1278's SCK
#define MISO 19	   // GPIO19 -- SX1278's MISnO
#define MOSI 27	   // GPIO27 -- SX1278's MOSI
#define SS 18	   // GPIO18 -- SX1278's CS
#define RST 14	   // GPIO14 -- SX1278's RESET
#define DI0 26	   // GPIO26 -- SX1278's IRQ(Interrupt Request)
#define BAND 433E6 // frequency in Hz (433E6, 868E6, 915E6)

#include <rom/rtc.h>

#include "Arduino.h"
#include <Button2.h>

void btnPressed(Button2 &btn);
void btnDownLongClickDetected(Button2 &btn);
void btnDownDoubleClick(Button2 &btn);

// battery measurement
// #define CONV_FACTOR 1.7
// #define READS 20
Pangodream_18650_CL BL(35); // pin 34 old / 35 new v2.1 hw

// Buttons for state machine control
#define BUTTON_UP 15   // up
#define BUTTON_DOWN 12 // down, 4 old / 12 new v2.1 hw

Button2 btnUp = Button2(BUTTON_UP);
Button2 btnDown = Button2(BUTTON_DOWN);

uint8_t vescBattery = 0;
uint8_t vescTempMotor = 0;

#include "common.h"

struct WinchToRemoteData w2r;
struct RemoteToWinchData r2w;

unsigned long lastTxLoraMessageMillis = 0; // last message send
unsigned long lastRxLoraMessageMillis = 0; // last message received
unsigned long previousRxLoraMessageMillis = 0;

unsigned int loraErrorCount = 0;
unsigned long loraErrorMillis = 0;

// Olles
#define PIN_PWM_IN 12
#define PWM_CYCLE_TIME_CONST 400500

#define T_SCREEN 100200
#define T_LORA_TX 203000
#define T_PWM_DROPOUT 80000
#define T_PWM_CYCLE_THRESH 20200
#define T_BTN_LOOP 10500

float PWM_CYCLE_FILTCOEFF;

unsigned long t_now, t_prev;
unsigned long t_prev_screen, t_prev_lora_tx;
unsigned long t_pwm_rise, t_pwm_fall, t_pwm_last, t_pwm_high, t_pwm_high_temp;
unsigned long t_pwm_diff, t_pwm_diff_f;
unsigned long t_btn_prev;
unsigned long tx_time;
bool pwm_state, pwm_state_prev, pwm_flag;
bool pwm_active, pwm_dropout;
bool lora_tx_timer, tx_enable;

unsigned long tl_0, tl_last, tl_diff;

uint8_t cnt, btnDownLongCnt, btnUpDoubleCnt;

void pwm_poll();
void pwm_process();
void pwm_state_change();

void update_screen();

void setup()
{
	Serial.begin(115200);

	// LoRa
	SPI.begin(SCK, MISO, MOSI, SS);
	LoRa.setPins(SS, RST, DI0);
	while (!LoRa.begin(BAND))
	{
		Serial.println("Starting LoRa failed!");
		delay(1000);
	}
	Serial.println("LoRa started");
	LoRa.setTxPower(20, PA_OUTPUT_PA_BOOST_PIN);
	LoRa.enableCrc();

	// Buttons
	btnUp.setPressedHandler(btnPressed);
	btnDown.setPressedHandler(btnPressed);
	btnDown.setLongClickTime(500);
	btnDown.setLongClickDetectedHandler(btnDownLongClickDetected);
	btnDown.setDoubleClickTime(400);
	btnDown.setDoubleClickHandler(btnDownDoubleClick);

	// Display
	display.init();
	display.flipScreenVertically();
	display.clear();

	// PWM
	pinMode(PIN_PWM_IN, INPUT);
	pwm_state = digitalRead(PIN_PWM_IN);
	PWM_CYCLE_FILTCOEFF = exp(float(-T_LORA_TX) / float(PWM_CYCLE_TIME_CONST));
	pwm_state_prev = pwm_state;
	t_now = micros();
	t_prev = t_now;
	t_pwm_last = t_now - T_PWM_DROPOUT;

	// Transmitter id
	r2w.id = myID;
	r2w.currentState = 11;
}

void loop()
{
	t_now = micros();

	lora_tx_timer = false;
	if (t_now - t_prev_lora_tx >= T_LORA_TX)
	{
		lora_tx_timer = true;
		t_prev_lora_tx = t_now;
	}

	pwm_poll();
	pwm_process();

	if (t_now - t_prev_screen >= T_SCREEN)
	{
		update_screen();
		t_prev_screen = t_now;
	}

	if (false) // t_now - t_btn_prev >= T_BTN_LOOP)
	{
		btnDown.loop();
		btnUp.loop();
		cnt++;
		t_btn_prev = t_now;
	}

	if (lora_tx_timer & tx_enable)
	{
		{
			r2w.currentState = 11;
			// ... prep setValue
			if (LoRa.beginPacket())
			{
				LoRa.write((uint8_t *)&r2w, sizeof(r2w));
				LoRa.endPacket();
				lastTxLoraMessageMillis = millis();
				tl_0 = micros();
				tl_diff = (tl_0 - tl_last) * 0.001;
				tl_last = tl_0;
			}
			lora_tx_timer = false;
		}
	}
}

void btnPressed(Button2 &btn)
{
	if (btn == btnUp)
	{
		;
	}
	else if (btn == btnDown)
	{
		;
	}
}

void btnDownLongClickDetected(Button2 &btn)
{
	btnDownLongCnt++;
}
void btnDownDoubleClick(Button2 &btn)
{
	btnUpDoubleCnt++;
}

void update_screen()
{
	display.clear();
	display.setTextAlignment(TEXT_ALIGN_LEFT);
	display.setFont(ArialMT_Plain_24); // 10, 16, 24
	display.drawString(0, 0, String(r2w.setTorque));
	display.drawString(0, 20, String(tl_diff));
	display.drawString(0, 40, String(t_pwm_diff_f));
	display.display();
}

// Update pwm
void pwm_process()
{
	if (pwm_flag)
	{
		pwm_active = true;
		pwm_dropout = false;
		t_pwm_high_temp = t_pwm_fall - t_pwm_rise;
		if (t_pwm_high_temp < 2300){
			t_pwm_high = t_pwm_high_temp;
			r2w.setTorque = (int8_t) map(constrain(t_pwm_high, 875, 2075), 875, 2075, -127, 127);
		}
	}
	else if ((t_now - t_pwm_last) >= T_PWM_DROPOUT)
	{
		pwm_active = false;
		pwm_dropout = true;
	}

	if (pwm_flag || pwm_dropout)
	{
		pwm_flag = false;
		pwm_dropout = false;

		t_pwm_diff = t_now - t_pwm_last;
		t_pwm_last = t_now;
	}

	if (lora_tx_timer)
	{
		t_pwm_diff_f = t_pwm_diff_f * PWM_CYCLE_FILTCOEFF + t_pwm_diff * (1 - PWM_CYCLE_FILTCOEFF);
	}

	if (t_pwm_diff_f < T_PWM_CYCLE_THRESH)
	{
		tx_enable = true;
	}
	else
	{
		tx_enable = false;
	}
}

// Temp fcn for polling pwm becasue of limited amount of concurrent PCINT pins
void pwm_poll()
{
	pwm_state = digitalRead(PIN_PWM_IN);
	if (pwm_state != pwm_state_prev)
	{
		pwm_state_prev = pwm_state;
		pwm_state_change();
	}
}

// PWM pin change interrupt
void pwm_state_change()
{
	if (pwm_state)
	{
		t_pwm_rise = micros();
	}
	else
	{
		t_pwm_fall = micros();
		pwm_flag = true;
	}
}
