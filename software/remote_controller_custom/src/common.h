struct LoraTxMessage
{
	uint8_t id : 4;			 // unique id 1 - 15, id 0 is admin!
	int8_t currentState : 4; // -2 --> -2 = hard brake -1 = soft brake, 0 = no pull / no brake, 1 = default pull (2kg), 2 = pre pull, 3 = take off pull, 4 = full pull, 5 = extra strong pull
	int8_t pullValue;		 // target pull value,  -127 - 0 --> 5 brake, 0 - 127 --> pull
	int8_t pullValueBackup;	 // to avoid transmission issues, TODO remove, CRC is enough??
};

// send by receiver (acknowledgement)
struct LoraRxMessage
{
	int8_t pullValue;	// currently active pull value,  -127 - 0 --> 5 brake, 0 - 127 --> pull
	uint8_t tachometer; // *10 --> in meter
	uint8_t dutyCycleNow;
	uint8_t vescBatteryOrTempMotor : 1;		 // 0 ==> vescTempMotor , 1 ==> vescBatteryPercentage
	uint8_t vescBatteryOrTempMotorValue : 7; // 0 - 127
};

struct RemoteToWinchData
{
	uint8_t id;			 // transmitter id
	int8_t currentState; // state of transmitter
	int8_t setTorque;	 // unscaled set value. [127 / max torque]
};

struct WinchToRemoteData
{
	uint8_t id;		 // acknowledged id
	int8_t fbTorque; // unscaled feedback value. [127 / max torque]
};